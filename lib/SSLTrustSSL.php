<?php
namespace WHMCS\Module\Server\Ssltrust;

use WHMCS\Module\Addon\SsltrustAdmin\SSLTrustAPI;
use WHMCS\Database\Capsule;
use WHMCS\Module\Addon\SsltrustAdmin\Common;

class SSLTrustSSL {

	const CONFIGURATION = 'Awaiting Configuration';
	const PENDING    	= 'Pending';
	const COMPLETE      = 'Completed';
	const REJECTED      = 'Rejected';
	const REVOKED       = 'Revoked';
	const EXPIRED       = 'Expired';
	const CANCELED      = 'Canceled';

	const CONFIG_PRODUCT      		= 'configoption1';
	const CONFIG_TERM      			= 'configoption2';
	const CONFIG_MULTI_DOMAIN       = 'configoption3';
	const CONFIG_VERIFICATION_TYPE  = 'configoption4';
	const CONFIG_SAN  				= 'configoption5';
	const CONFIG_WSAN 				= 'configoption6';
	const CONFIG_BRAND 				= 'configoption7';
	const CONFIG_ADDITIONAL_SAN 	= 'additional_domains';
	const CONFIG_ADDITIONAL_WSAN 	= 'wildcard_domains';

	const LINK_EXPIRY 				= 86400;
	const RENEWAL_BLOCK_DAYS 		= 90; // this is to stop re-ordering a service already activated ( problem with WHMCS )

	public function __construct() {
		
	}

	public function getProducts()
    {
        return SSLTrustAPI::getProducts();
    }

	public function order($order, $params)
    {
		$results = SSLTrustAPI::newOrder($order);
		if ($results && $results['status'] === 'success') {   
			Capsule::table('tblsslorders')->insert([
				'userid' => $params['clientsdetails']['userid'],
				'serviceid'=>  $params['serviceid'],
				'remoteid'=>  $results['response']['service'],
				'module'=>  'ssltrust',
				'certtype'=>  $order['pid'],
				'status'=>  'Awaiting Configuration',
				'configdata'=>  ''
			  ]);
			  Capsule::table('mod_ssltrust_orders')->insert([
				'whmcs_service_id'=>  $params['serviceid'],
				'whmcs_package_id'=>  $params['packageid'],
				'ssltrust_serviceid'=>  $results['response']['service'],
				'san'=>  $order['san'],
				'wsan'=>  $order['wsan'],
			  ]);
		}
		return $results;
    }

	public function renewal($order, $params)
    {
		$results = SSLTrustAPI::newOrder($order);
		if ($results && $results['status'] === 'success') {   
			Capsule::table('tblsslorders')
			 ->where('serviceid', $params['serviceid'])
			 ->update([
				'remoteid'=>  $results['response']['service'],
				'module'=>  'ssltrust',
				'certtype'=>  $order['pid'],
				'status'=>  'Awaiting Configuration',
				'configdata'=>  ''
			  ]);
			Capsule::table('mod_ssltrust_orders')
			  ->where('whmcs_service_id', $params['serviceid'])
			  ->update([
				'whmcs_package_id'=>  $params['packageid'],
				'ssltrust_serviceid'=>  $results['response']['service'],
				'san'=>  $order['san'],
				'wsan'=>  $order['wsan'],
				'ordered' => date("Y-m-d H:i:s")
			  ]);
		}
		return $results;
    }

	public function checkAndProcessUpgrade($params, $orderData = NULL) // full params required
    {
		if (SSLTrustSSL::hasUpgradesToProcess($params['serviceid']) && $params[SSLTrustSSL::CONFIG_MULTI_DOMAIN] === 'on') {
			if (!$orderData) {
				$orderData = SSLTrustSSL::getLocalOrder($params['serviceid']);
			}
			$includedSan = intval($params[SSLTrustSSL::CONFIG_SAN]);
			$includedWSan = intval($params[SSLTrustSSL::CONFIG_WSAN]);

			$additionalSan = intval($params['configoptions'][SSLTrustSSL::CONFIG_ADDITIONAL_SAN]);
			$additionalWSan = intval($params['configoptions'][SSLTrustSSL::CONFIG_ADDITIONAL_WSAN]);
			
			$upgradeOrder = SSLTrustSSL::upgradeOrder([
			'service' => intval($orderData->ssltrust_serviceid),
			'san' => intval($includedSan + $additionalSan),
			'wsan' => intval($includedWSan + $additionalWSan) 
			], $params);

			if (!$upgradeOrder || $upgradeOrder['status'] != 'success') {   
				throw new \Exception('Error processing pending upgrade with Certificate Provider. Please contact Support and provide the following; Code: ' . $upgradeOrder['code'] . ' , Status:' . $upgradeOrder['statuscode']);
			}
			SSLTrustSSL::clearUpgradesToProcess($params['serviceid']);
		}
	}

	public function upgradeOrder($order, $params)
    {
		$results = SSLTrustAPI::upgradeOrder($order);
		if ($results && $results['status'] === 'success') {   
			  Capsule::table('mod_ssltrust_orders')
			  ->where('whmcs_service_id', $params['serviceid'])
			  ->update([
				'san'=>  $order['san'],
				'wsan'=>  $order['wsan']
			  ]);
		}
		return $results;
    }

	public function hasUpgradesToProcess($serviceid) {
		$upgrades = Capsule::table('mod_ssltrust_upgrades')
					->where('whmcs_service_id', '=', $serviceid)
					->first();
		return boolval($upgrades);
	}

	public function clearUpgradesToProcess($serviceid) {
		Capsule::table('mod_ssltrust_upgrades')
				->where('whmcs_service_id', '=', $serviceid)
				->delete();
	}

	public function getLocalOrder($serviceid)
    {
        return Capsule::table('mod_ssltrust_orders')
					->leftJoin('tblsslorders', 'mod_ssltrust_orders.whmcs_service_id', '=', 'tblsslorders.serviceid')
					->where('whmcs_service_id', '=', $serviceid)
					->first();
    }

	public function getService($serviceid)
    {
		$service = SSLTrustAPI::getService($serviceid);
		if ($service['status'] !== 'success' || !$service['response']['vendor']) {
			//return $service;
		}
		switch ($service['response']['vendor']['status']) {
			case 'rejected':
			  $currentStatus = SSLTrustSSL::REJECTED;
			  break;
			case 'issued':
			  $currentStatus = SSLTrustSSL::COMPLETE;
			  break;
			case 'revoked':
			  $currentStatus = SSLTrustSSL::REVOKED;
			  break;
			case 'canceled':
			  $currentStatus = SSLTrustSSL::CANCELED;
			  break;
			case 'pending_configuration':
			  $currentStatus = SSLTrustSSL::CONFIGURATION;
			  break;
			case 'reissue_pending':
			case 'pending':
			case 'processing':
			case 'needs_csr':
			case 'needs_approval':
			  $currentStatus = SSLTrustSSL::PENDING;
			  break;
			case 'expired':
			  $currentStatus = SSLTrustSSL::EXPIRED;
			  break;
		}
		$service['response']['vendor']['status'] = $currentStatus;
		return $service;
    }

	public function generate_config_url($brand, $id, $expiry = SSLTrustSSL::LINK_EXPIRY)
    {
        return Common::generate_config_url($brand, $id, $expiry);
    }

	public function generate_reissue_url($brand, $id, $expiry = SSLTrustSSL::LINK_EXPIRY)
    {
        return Common::generate_reissue_url($brand, $id, $expiry);
    }
	
	public function generate_collect_url($brand, $id, $expiry = SSLTrustSSL::LINK_EXPIRY)
    {
        return Common::generate_collect_url($brand, $id, $expiry);
    }

	public function generate_domains_url($brand, $id, $expiry = SSLTrustSSL::LINK_EXPIRY)
    {
        return Common::generate_domains_url($brand, $id, $expiry);
    }

	public function generate_organisations_url($brand, $id, $expiry = SSLTrustSSL::LINK_EXPIRY)
    {
        return Common::generate_organisations_url($brand, $id, $expiry);
    }

	public function generate_validationmanager_url($brand, $id, $expiry = SSLTrustSSL::LINK_EXPIRY)
    {
        return Common::generate_validationmanager_url($brand, $id, $expiry);
    }

	public function setServiceStatus($serviceid, $status)
    {
      Capsule::table('tblsslorders')
                  ->where('serviceid', $serviceid)
                  ->update(['status' => $status]);

    }

	/**
	 * 
	 * Private Functions
	 * 
	 */

}


?>
