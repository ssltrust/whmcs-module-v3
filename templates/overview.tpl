{include file='./styles.tpl'}
<div class="row text-left">
<div class="col-sm-12">
<h2 style="margin-top: 0;">{$productName}</h2>
{if $status eq 'Awaiting Configuration'}
    {if $serviceDomain}<h3>{$serviceDomain}</h3>{/if}
    <p><strong>{$LANG.ssltrust_awaiting_configuration_title}</strong><br>
      {$LANG.ssltrust_awaiting_configuration_body}
       <br><br>
       <a class="btn btn-primary" target="_blank"
            href="{$linkConfigure}">
            {$LANG.ssltrust_start_configuration}
        </a>
        {if $multiDomainProduct eq "on" && $allowUpgrades eq 1}
        <a class="btn btn-info"
            href="upgrade.php?type=configoptions&id={$serviceid}">
            {$LANG.ssltrust_add_san}
        </a>
        {/if}
        {if $verficationType neq 'DV'}
        <br><br>
        <a class="btn btn-info" target="_blank"
            href="{$linkOrganisations}">
            {$LANG.ssltrust_manage_organisations}
        </a>
        <a class="btn btn-info" target="_blank"
            href="{$linkDomains}">
            {$LANG.ssltrust_manage_domains}
        </a>
        {/if}
    </p>
{elseif $status eq 'Pending'}
    <p>{$LANG.ssltrust_validating_stage}</p>
    {if $verficationType eq 'DV'}
      <p>{$LANG.ssltrust_complete_domain_validation}</p>
      <a class="btn btn-primary" target="_blank"
          href="{$linkValidation}">
          {$LANG.ssltrust_access_validation_manager}
      </a>
    {elseif $verficationType eq 'OV'}
      <p>{$LANG.ssltrust_complete_domain_business_validation}</p>
      <a class="btn btn-primary" target="_blank"
          href="{$linkValidation}">
          Access Validation Manager
      </a>
    {elseif $verficationType eq 'EV'}
      <p>{$LANG.ssltrust_complete_domain_business_validation}</p>
      <a class="btn btn-primary" target="_blank"
          href="{$linkValidation}">
          Access Validation Manager
      </a>
    {/if}
{elseif $status eq 'Completed'}
    <h3><span class="label label-success"></span></h3>
    <p>{$LANG.ssltrust_certificate_ready_collection}</p>
    <a class="btn btn-primary" target="_blank"
        href="{$linkCollection}">
        {$LANG.ssltrust_collect_download}
    </a>
    <a class="btn btn-info" target="_blank"
        href="{$linkReissue}">
        {$LANG.ssltrust_reissue_reconfigure}
    </a>
    {if $multiDomainProduct eq "on" && $allowUpgrades eq 1}
    <a class="btn btn-info"
        href="upgrade.php?type=configoptions&id={$serviceid}">
        {$LANG.ssltrust_add_san}
    </a>
    {/if}
{elseif $status eq 'Revoked'}
  <h3><span class="label label-warning">{$LANG.ssltrust_revoked_title}</span></h3>
  <p>{$LANG.ssltrust_revoked_body}</p>
{elseif $status eq 'Rejected'}
   <h3><span class="label label-warning">{$LANG.ssltrust_rejected_title}</span></h3>
   <p>{$LANG.ssltrust_rejected_body}</p>
{else}
  <div class="alert alert-warning" role="alert">
    
  </div>
{/if}

{if $vendor}
  {if $status eq 'Completed'}
    <div class="well cert-details-wrapper">
        <table width="100%" border="0">
        <tbody>
          <tr>
            <th scope="row">{$LANG.ssltrust_common_name}</th>
            <td>
              {if $vendor.certificate.common_name }
                {$vendor.certificate.common_name}
              {else}
                None
              {/if}
            </td>
          </tr>
          <tr>
            <th scope="row">{$LANG.ssltrust_san}</th>
            <td>{', '|implode:$vendor.certificate.dns_names}</td>
          </tr>
          <tr>
            <th scope="row">{$LANG.ssltrust_valid_from}</th>
            <td>{$vendor.certificate.valid_from}</td>
          </tr>
          <tr>
            <th scope="row">{$LANG.ssltrust_expires}</th>
            <td>{$vendor.certificate.valid_till}</td>
          </tr>
          <tr>
            <th scope="row">{$LANG.ssltrust_signature_type}</th>
            <td>{$vendor.certificate.signature_hash}</td>
          </tr>
          <tr>
            <th scope="row">{$LANG.ssltrust_service_end}</th>
            <td>{$vendor.order_valid_till}</td>
          </tr>
        </tbody>
      </table>
    </div>
  {/if}
{/if}
</div>
</div>
