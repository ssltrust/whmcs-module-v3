<?php
if (!defined("WHMCS")) {
    die("This file cannot be accessed directly");
}
## in module
$_LANG['ssltrust_service_not_found'] = 'Service not found';
$_LANG['ssltrust_error_getting_status'] = 'Error getting service status from Certificate Provider. Please contact support if this error continues.';
$_LANG['ssltrust_service_not_activated'] = 'Service not yet activated with Certificate Provider. Please contact support if this error continues.';
$_LANG['ssltrust_service_unavailable'] = 'Service currently unavailable from Certificate Provider. Please contact support if this error continues.';
$_LANG['ssltrust_manage_organisations'] = 'Manage Account Organisations';
$_LANG['ssltrust_manage_domains'] = 'Manage Account Domains';
$_LANG['ssltrust_already_exists'] = 'Sorry an order already exists with this Service ID';
$_LANG['ssltrust_error_creation'] = 'Error creating service with supplier.';
$_LANG['ssltrust_not_found_upgrade'] = 'Service not found to upgrade in Certificate Providers database table.';
$_LANG['ssltrust_error_upgrading'] = 'Error upgrading service with Certificate Provider.';
$_LANG['ssltrust_error_already_activated'] = "The order was already ordered recently. Should not be renewing.";

## in template
$_LANG['ssltrust_certificate_issued'] = 'Certificate Issued';
$_LANG['ssltrust_certificate_ready_collection'] = 'Your Certificate is ready for Collection.';
$_LANG['ssltrust_collect_download'] = 'Collect/Download Certificate';
$_LANG['ssltrust_reissue_reconfigure'] = 'Reissue/Reconfigure Certificate';
$_LANG['ssltrust_add_san'] = 'Add Additional Domain/SAN Slots';
$_LANG['ssltrust_awaiting_configuration_title'] = 'Your Certificate is awaiting configuration!';
$_LANG['ssltrust_awaiting_configuration_body'] = 'Click \'Start Certificate Configuration\' to get started with your Certificate configuration:';
$_LANG['ssltrust_start_configuration'] = 'Start Certificate Configuration';
$_LANG['ssltrust_validating_stage'] = 'Your Certificate is in the Validation stage.';
$_LANG['ssltrust_complete_domain_validation'] = 'You must complete Domain Control Validation to have your new Certificate issued.';

$_LANG['ssltrust_access_validation_manager'] = 'Access Validation Manager';
$_LANG['ssltrust_complete_domain_business_validation'] = 'You must complete Domain Control Validation and Organisation Validation to have your new Certificate issued.<br>Please access the Validation Manager to check on the current status and complete any required tasks.';
$_LANG['ssltrust_revoked_title'] = 'Certificate Revoked';
$_LANG['ssltrust_revoked_body'] = 'Your certificate has been revoked. <br>Please contact our Support Team for more details and to progress with a new configuration if required.';
$_LANG['ssltrust_rejected_title'] = 'Certificate Rejected';
$_LANG['ssltrust_rejected_body'] = 'Your Certificate Request has been rejected.<br>Please contact our Support Team for more details and to progress with a new configuration if required.';
$_LANG['ssltrust_service_unavailable'] = 'Awaiting service is currently unavailable. Please contact our Support Team for additional information.';
$_LANG['ssltrust_common_name'] = 'Common Name:';
$_LANG['ssltrust_san'] = 'SAN:';
$_LANG['ssltrust_valid_from'] = 'Certificate Valid From:';
$_LANG['ssltrust_expires'] = 'Certificate Expires:';
$_LANG['ssltrust_signature_type'] = 'Signature Type:';
$_LANG['ssltrust_service_end'] = 'Service End:';

