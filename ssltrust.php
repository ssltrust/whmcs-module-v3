<?php
//
// for any support please contact support@ssltrust.com
// this module requires the SSLTrust Admin Module to be installed
//
if (!defined("WHMCS")) {
    die("This file cannot be accessed directly");
}

use WHMCS\Module\Server\Ssltrust\SSLTrustSSL;
use WHMCS\Database\Capsule;
use WHMCS\Module\Addon\SsltrustAdmin\Common;

function ssltrust_MetaData()
{
    return array(
        'DisplayName' => 'SSLTrust Provisioning Module',
        'APIVersion' => '1.2',
        'RequiresServer' => false,
    );
}

function ssltrust_ClientAreaCustomButtonArray($params) {
  global $_LANG;
  ssltrust_initlang();
  return [$_LANG['ssltrust_manage_organisations'] => "OrganisationManager",
              $_LANG['ssltrust_manage_domains'] => "DomainManager"];

}

function ssltrust_ConfigOptions() {
  $products = SSLTrustSSL::getProducts();
  $productList = array();
  if ($products['status'] === 'success' && !empty($products['response'])) {
    usort($products['response'], function($a, $b) {return strcmp($a["name"], $b["name"]);});
    foreach ($products['response'] as $product) {
        array_push($productList, ($product['name'] . " #" . $product['id']));
    }
    $productList = join(',', $productList);
  } else {
    $productList = 'Error getting product list';
  }
  $config = array(
                  'Certificate Type' => [
                    'Type' =>'dropdown',
                    'Options' => $productList,
                    "Description" => "<br>SSLTrust Product",
                  ],
                  'Months' => [ 
                    'Type' => 'dropdown',
                    'Options' => '12,24,36,48,60,72',
                    "Description" => "<br>Product Term Length (make sure you select a valid term for the product)",
                  ],
                  "multi_domain" => [
                    "FriendlyName" => "Multi-domain",
                    "Description" => "Does this product allow multiple domains?",
                    "Type" => "yesno",
                  ],
                  "verification_type" => [
                    "FriendlyName" => "Verification Type",
                    "Type" => "dropdown",
                    "Options" => [
                        'DV' => 'DV',
                        'OV' => 'OV',
                        'EV' => 'EV',
                    ],
                   "Default" => "DV",
                  ],
                  "included_additional_domains" => [
                          "FriendlyName" => "Included Standard Domains",
                          "Description" => "<br>Number of additional included domains (not including the CN)",
                          "Type" => "text",
                  ],
                  "included_additional_wildcard_domains" => [
                    "FriendlyName" => "Included Wildcard Domains",
                    "Description" => "<br>Number of additional included wildcard domains (not including the CN)",
                    "Type" => "text",
                  ],
                  "branding" => [
                    "FriendlyName" => "Branding",
                    "Type" => "dropdown",
                    "Options" => [
                        'digicert' => 'Digicert',
                        'geotrust' => 'GeoTrust',
                        'thawte' => 'Thawte',
                        'rapidssl' => 'RapidSSL',
                        'verokey' => 'Verokey',
                    ],
                   "Default" => "digicert",
                  ]);
  return $config;
}


function ssltrust_CreateAccount($params) {
  try {
    global $_LANG;
    ssltrust_initlang();
    $orderData = SSLTrustSSL::getLocalOrder($params['serviceid']);
    if($orderData != NULL){
      throw new Exception($_LANG['ssltrust_already_exists']);
    }

    $prodId = explode('#', $params[SSLTrustSSL::CONFIG_PRODUCT])[1];
    $term = $params[SSLTrustSSL::CONFIG_TERM];
    $multiDomain = $params[SSLTrustSSL::CONFIG_MULTI_DOMAIN];
    if ($multiDomain === 'on') {
      $includedSan = intval($params[SSLTrustSSL::CONFIG_SAN]);
      $includedWSan = intval($params[SSLTrustSSL::CONFIG_WSAN]);

      $additionalSan = intval($params['configoptions'][SSLTrustSSL::CONFIG_ADDITIONAL_SAN]);
      $additionalWSan = intval($params['configoptions'][SSLTrustSSL::CONFIG_ADDITIONAL_WSAN]);

      $totalSan = $includedSan + $additionalSan;
      $totalWSan = $includedWSan + $additionalWSan;
    } else {
      $totalSan = 0;
      $totalWSan = 0;
    }
    $newOrder = SSLTrustSSL::order([
                        'pid' => intval($prodId),
                        'term'=> intval( $term),
                        'container' => $params['userid'],
                        'san' => intval($totalSan),
                        'wsan' => intval($totalWSan) 
                      ], $params);
    
    if (!$newOrder || $newOrder['status'] != 'success') {   
      throw new Exception($newOrder['message']?:$_LANG['ssltrust_error_creation']);
    }
    SSLTrustSSL::clearUpgradesToProcess($params['serviceid']);
  } catch (Exception $e) {
    logModuleCall(
        'ssltrust',
        __FUNCTION__,
        $params,
        $e->getMessage(),
        $e->getTraceAsString()
    );
    return $e->getMessage();
  }
  return 'success';
}

function ssltrust_ChangePackage($params) {
  // We have to do this a bit different due to the weird way WHMCS does upgrades
  try {
    global $_LANG;
    ssltrust_initlang();
    $orderData = SSLTrustSSL::getLocalOrder($params['serviceid']);
    if (!$orderData) {
      throw new Exception($_LANG['ssltrust_not_found_upgrade']);
    }
    if ($params[SSLTrustSSL::CONFIG_MULTI_DOMAIN] === 'on') {
      $serviceData = SSLTrustSSL::getService($orderData->ssltrust_serviceid);
      if ($serviceData['status'] !== 'success') {
        throw new Exception($_LANG['ssltrust_error_getting_status']);
      }
      if ($serviceData['response']['status'] !== 'Active' && $serviceData['response']['status'] !== 'Completed') {
        throw new Exception($_LANG['ssltrust_service_not_activated']);
      }

      $includedSan = intval($params[SSLTrustSSL::CONFIG_SAN]);
      $includedWSan = intval($params[SSLTrustSSL::CONFIG_WSAN]);
      $additionalSan = intval($params['configoptions'][SSLTrustSSL::CONFIG_ADDITIONAL_SAN]);
      $additionalWSan = intval($params['configoptions'][SSLTrustSSL::CONFIG_ADDITIONAL_WSAN]);
      // check to see if an upgrade is actually needed.
      if (($includedSan + $additionalSan) !== intval($serviceData['response']['san']) || ($includedWSan + $additionalWSan) !== intval($serviceData['response']['wsan'])) {
        // Order has SAN, lets add it to the queue....
        Capsule::table('mod_ssltrust_upgrades')->insert([
          'whmcs_service_id'=>  $params['serviceid'],
          'whmcs_package_id'=>  $params['packageid'],
        ]);
      }
    }
  } catch (Exception $e) {
    logModuleCall(
        'ssltrust',
        __FUNCTION__,
        $params,
        $e->getMessage(),
        $e->getTraceAsString()
    );
    return $e->getMessage();
  }
  return 'success';
}

function ssltrust_Renew($params) {
  // we do a check to make sure a renewal or order has already been done recently
  // this is due to a problem with WHMCS doing multiple orders
  return ssltrust_renewalProcess($params, false);
}

function ssltrust_adminForceRenewal(array $params)
{
  // admin is wanting to do a new order without checks for recent orders
  return ssltrust_renewalProcess($params, true);
}

function ssltrust_renewalProcess(array $params, $force = false)
{
  try {
    global $_LANG;
    ssltrust_initlang();
    $orderData = SSLTrustSSL::getLocalOrder($params['serviceid']);
    $today = time();
    $lastOrder = strtotime($orderData->ordered) + (SSLTrustSSL::RENEWAL_BLOCK_DAYS * 60 * 60 * 24);
    if ($today <= $lastOrder && !$force){
       //dont do another within X days of last order, a small check to help prevent WHMCS quirks
      return $_LANG['ssltrust_error_already_activated'];
    }
    $prodId = explode('#', $params[SSLTrustSSL::CONFIG_PRODUCT])[1];
    $term = $params[SSLTrustSSL::CONFIG_TERM];
    $multiDomain = $params[SSLTrustSSL::CONFIG_MULTI_DOMAIN];
    if ($multiDomain === 'on') {
      $includedSan = intval($params[SSLTrustSSL::CONFIG_SAN]);
      $includedWSan = intval($params[SSLTrustSSL::CONFIG_WSAN]);

      $additionalSan = intval($params['configoptions'][SSLTrustSSL::CONFIG_ADDITIONAL_SAN]);
      $additionalWSan = intval($params['configoptions'][SSLTrustSSL::CONFIG_ADDITIONAL_WSAN]);

      $totalSan = $includedSan + $additionalSan;
      $totalWSan = $includedWSan + $additionalWSan;
    } else {
      $totalSan = 0;
      $totalWSan = 0;
    }
    $newOrder = SSLTrustSSL::renewal([
                        'pid' => intval($prodId),
                        'term'=> intval( $term),
                        'container' => $params['userid'],
                        'san' => intval($totalSan),
                        'wsan' => intval($totalWSan) 
                      ], $params);
    
    if (!$newOrder || $newOrder['status'] != 'success') {   
      throw new Exception($newOrder['message']?:$_LANG['ssltrust_error_creation']);
    }
    SSLTrustSSL::clearUpgradesToProcess($params['serviceid']);
  } catch (Exception $e) {
    logModuleCall(
        'ssltrust',
        __FUNCTION__,
        $params,
        $e->getMessage(),
        $e->getTraceAsString()
    );
    return $e->getMessage();
  }
  return 'success';
}

function ssltrust_ClientArea($params) {
  try {
    global $_LANG;
    ssltrust_initlang();
    if ($params['status'] === 'Active' || $params['status'] === 'Completed') {
      $orderData = SSLTrustSSL::getLocalOrder($params['serviceid']);
      SSLTrustSSL::checkAndProcessUpgrade($params, $orderData);
      if ($_GET['a']) {
        switch ($_GET['a']) {
          case 'ConfigurationManager':
            ssltrust_ConfigurationManager($params);
            break;
          case 'ValidationManager':
            ssltrust_ValidationManager($params);
            break;
          case 'CollectionManager':
            ssltrust_CollectionManager($params);
            break;
          case 'ReissueManager':
            ssltrust_ReissueManager($params);
            break;
        }
      }
      if (!$orderData) {
        throw new Exception($_LANG['ssltrust_service_not_found']);
      }
      $serviceData = SSLTrustSSL::getService($orderData->ssltrust_serviceid);
      if ($serviceData['status'] !== 'success') {
        throw new Exception($_LANG['ssltrust_error_getting_status']);
      }
      if ($serviceData['response']['status'] !== 'Active' && $serviceData['response']['status'] !== 'Completed') {
        throw new Exception($_LANG['ssltrust_service_not_activated']);
      }
      if (!$serviceData['response']['vendor']) {
        throw new Exception($_LANG['ssltrust_service_unavailable']);
      }
      SSLTrustSSL::setServiceStatus($params['serviceid'], $serviceData['response']['vendor']['status']);
      $templateVars = ssltrust_defaultTemplateParams($params);
      return [
        'templatefile' => 'templates/overview',
        'vars' => array_merge($templateVars, [
          'status' => $serviceData['response']['vendor']['status']?:'Unknown',
          'vendor' => $serviceData['response']['vendor']['status'] === 'Completed' ? $serviceData['response']['vendor'] : NULL,
          'linkConfigure' => "clientarea.php?action=productdetails&id=" . $params['serviceid'] . "&a=ConfigurationManager",
          'linkValidation' => "clientarea.php?action=productdetails&id=" . $params['serviceid'] . "&a=ValidationManager",
          'linkDomains' => "clientarea.php?action=productdetails&id=" . $params['serviceid'] . "&modop=custom&a=DomainManager",
          'linkOrganisations' => "clientarea.php?action=productdetails&id=" . $params['serviceid'] . "&modop=custom&a=OrganisationManager",
          'linkCollection' => "clientarea.php?action=productdetails&id=" . $params['serviceid'] . "&a=CollectionManager",
          'linkReissue' => "clientarea.php?action=productdetails&id=" . $params['serviceid'] . "&a=ReissueManager",
        ])
      ];
    }
  } catch (Exception $e) {
    logModuleCall(
        'ssltrust',
        __FUNCTION__,
        $params,
        $e->getMessage(),
        $e->getTraceAsString()
    );
    return $e->getMessage();
  }
  return '';
}

function ssltrust_AdminServicesTabFields($params) {
  try {
    global $_LANG;
    ssltrust_initlang();
    if ($params['status'] === 'Active' || $params['status'] === 'Completed') {
      $orderData = SSLTrustSSL::getLocalOrder($params['serviceid']);
      SSLTrustSSL::checkAndProcessUpgrade($params, $orderData);
      if (!$orderData) {
        throw new Exception($_LANG['ssltrust_service_not_found']);
      }
      $serviceData = SSLTrustSSL::getService($orderData->ssltrust_serviceid);
      if ($serviceData['status'] !== 'success') {
        throw new Exception($_LANG['ssltrust_error_getting_status']);
      }
      if ($serviceData['response']['status'] !== 'Active' && $serviceData['response']['status'] !== 'Completed') {
        throw new Exception($_LANG['ssltrust_service_not_activated']);
      }
      if (!$serviceData['response']['vendor']) {
        throw new Exception($_LANG['ssltrust_service_unavailable']);
      }
      SSLTrustSSL::setServiceStatus($params['serviceid'], $serviceData['response']['vendor']['status']);
      
      $outputOrder = '<strong>Service ID: </strong>' . $orderData->ssltrust_serviceid;
      $outputOrder .= '<br><strong>Order ID: </strong>' . $serviceData['response']['order'];
      $outputOrder .= '<br><strong>Status: </strong>' . $serviceData['response']['status'];
      $outputOrder .= '<br><strong>Product: </strong>' . $serviceData['response']['name'];
      $outputOrder .= '<br><strong>Cost: </strong>' . $serviceData['response']['paymentAmount'];
      $outputOrder .= '<br><strong>SAN: </strong> Standard: ' . $serviceData['response']['san'] . ' , Wildcard: ' . $serviceData['response']['wsan'];

      if (!empty($serviceData['response']['vendor'])) {
        $outputCA = '<strong>Status: </strong>' . $serviceData['response']['vendor']['status'];
        $outputCA .= '<br><strong>Valid From: </strong>' . $serviceData['response']['vendor']['order_valid_from'];
        $outputCA .= '<br><strong>Valid Till: </strong>' . $serviceData['response']['vendor']['order_valid_till'];
        $outputCert = 'None';
        if (!empty($serviceData['response']['vendor']['certificate'])) {
          $outputCert = '<strong>Cert ID: </strong>' . $serviceData['response']['vendor']['certificate']['id'];
          $outputCert .= '<br><strong>Cert S/N: </strong>' . $serviceData['response']['vendor']['certificate']['serial_number'];
          $outputCert .= '<br><strong>Common Name: </strong>' . $serviceData['response']['vendor']['certificate']['common_name'];
          $outputCert .= '<br><strong>SANs: </strong>' . join($serviceData['response']['vendor']['certificate']['dns_names'], ', ');
          $outputCert .= '<br><strong>Valid From: </strong>' . $serviceData['response']['vendor']['certificate']['valid_from'];
          $outputCert .= '<br><strong>Valid Till: </strong>' . $serviceData['response']['vendor']['certificate']['valid_till'];
          $outputCert .= '<br><strong>Days Remaining: </strong>' . $serviceData['response']['vendor']['certificate']['days_remaining'];
        }
      } else {
        $outputCert = '<strong>Status: </strong>' . 'Unknown';
      }
      return array('SSLTrust Info' => $outputOrder, 'CA Order Info' => $outputCA, 'Certificate Info' => $outputCert);
    }
  } catch (Exception $e) {
    logModuleCall(
        'ssltrust',
        __FUNCTION__,
        $params,
        $e->getMessage(),
        $e->getTraceAsString()
    );
    if (strpos($e->getMessage(), 'UPGRADE_IN_PROGRESS') !== false) {
      return array('<b>ERROR</b>' => '<b>You have a pending upgrade that cant be processed on your SSLTrust Account.</b><br>An Invoice needs to be paid for an existing upgrade. Please login to your SSLTrust Account and make payment.');
    } else {
      return array('ERROR' => $e->getMessage());
    }
  }
}

function ssltrust_AdminCustomButtonArray()
{
    return array(
        "Remove Pending Upgrade Requests" => "adminRemovePending",
        "Force Renewal" => "adminForceRenewal",
    );
}

function ssltrust_adminRemovePending(array $params)
{
    try {
      SSLTrustSSL::clearUpgradesToProcess($params['serviceid']);
    } catch (Exception $e) {
      logModuleCall(
          'ssltrust',
          __FUNCTION__,
          $params,
          $e->getMessage(),
          $e->getTraceAsString()
      );
      return $e->getMessage();
    }
}

/*
 * Custom Client Area Menu Action - OrganisationManager
 */
function ssltrust_OrganisationManager(array $params)
{ 
    $orderData = SSLTrustSSL::getLocalOrder($params['serviceid']);
    header('Location: ' . SSLTrustSSL::generate_organisations_url($params[SSLTrustSSL::CONFIG_BRAND], $orderData->ssltrust_serviceid), true, 302);
    exit();
}

/*
 * Custom Client Area Menu Action - DomainManager
 */
function ssltrust_DomainManager(array $params)
{
    $orderData = SSLTrustSSL::getLocalOrder($params['serviceid']);
    header('Location: ' . SSLTrustSSL::generate_domains_url($params[SSLTrustSSL::CONFIG_BRAND], $orderData->ssltrust_serviceid), true, 302);
    exit();
}

function ssltrust_ConfigurationManager(array $params)
{
    $orderData = SSLTrustSSL::getLocalOrder($params['serviceid']);
    header('Location: ' . SSLTrustSSL::generate_config_url($params[SSLTrustSSL::CONFIG_BRAND], $orderData->ssltrust_serviceid), true, 302);
    exit();
}

function ssltrust_ReissueManager(array $params)
{
    $orderData = SSLTrustSSL::getLocalOrder($params['serviceid']);
    header('Location: ' . SSLTrustSSL::generate_reissue_url($params[SSLTrustSSL::CONFIG_BRAND], $orderData->ssltrust_serviceid), true, 302);
    exit();
}

function ssltrust_ValidationManager(array $params)
{
    $orderData = SSLTrustSSL::getLocalOrder($params['serviceid']);
    header('Location: ' . SSLTrustSSL::generate_validationmanager_url($params[SSLTrustSSL::CONFIG_BRAND], $orderData->ssltrust_serviceid), true, 302);
    exit();
}

function ssltrust_CollectionManager(array $params)
{
    $orderData = SSLTrustSSL::getLocalOrder($params['serviceid']);
    header('Location: ' . SSLTrustSSL::generate_collect_url($params[SSLTrustSSL::CONFIG_BRAND], $orderData->ssltrust_serviceid), true, 302);
    exit();
}

function ssltrust_defaultTemplateParams($params)
{
    return [
        'productName' => $params['model']['product']['name'],
        'multiDomainProduct' => $params[SSLTrustSSL::CONFIG_MULTI_DOMAIN],
        'verficationType' => $params[SSLTrustSSL::CONFIG_VERIFICATION_TYPE],
        'brand' => $params[SSLTrustSSL::CONFIG_BRAND],
        'serviceId' => $params['serviceid'],
        'serviceDomain' => $params['domain'],
        'allowUpgrades' => $params['templatevars']['configoptionsupgrade']
    ];
}

function ssltrust_initlang() {
  global $CONFIG, $_LANG, $smarty;
  $lang = !empty($_SESSION['Language']) ? $_SESSION['Language'] : $CONFIG['Language'];
  $langFile = dirname(__file__)."/lang/".$lang.".php";
  if (!file_exists($langFile)) $langFile = dirname(__file__)."/lang/".ucfirst($lang).".php";
  if (!file_exists($langFile)) $langFile = dirname(__file__)."/lang/english.php";
  require_once $langFile;
  if (isset($_MOD_LANG) && is_array($_MOD_LANG)){
      foreach ($_MOD_LANG as $k=>$v){
          if (empty($_LANG[$k])){
              $_LANG[$k]=$v;
          }
      }
  }
  if (isset($smarty)){
      $smarty->assign("LANG", $_LANG);
  }
}

?>
